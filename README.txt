Tools
1. Eclipse or any IDE
2. MySQL or any tools
3. Java 8
4. Maven

Database name : wcc_group  (declare on application.properties in resources folder)
Copy of Database in db folder

Compile -   mvn clean install -DskipTests=true
Run -       mvn spring-boot:run


WEB-Based URL    	http://localhost:8080/geographic-ui/
Rest-API URL		http://localhost:8080/geographic-ui/swagger-ui.html#
