/* jshint smarttabs: true */

/**
 * The URL link constant class. 
 *
 * @class
 * @name LinkConst
 **/
define([
], function() {
	'use strict';

	var APP_ROOT_CONTEXT = "";
	
	if (APP_ROOT_CONTEXT == '/') {
		APP_ROOT_CONTEXT = "";
	}
	
	var LinkConst = _.extend({}, /** @lends LinkConst.prototype */ {
		
		
		/**
		 * A list of Reconciliation link.
		 *
		 * @const {Object}
		 * @property {String} POST_MULTIPLE_FILE /reconciliation/file/upload
		 */
		Geographic : {
			GET_CALCULATE_DISTANCE				: APP_ROOT_CONTEXT + '/geographic-ui/api/geographic/calculate', //get
			GET_POSTCODE_LIST					: APP_ROOT_CONTEXT + '/geographic-ui/api/geographic/postcodelist', //get
			POST_UPDATE_POSTCODE				: APP_ROOT_CONTEXT + '/geographic-ui/api/geographic/update', //post
		}
	});

	return LinkConst;
});
