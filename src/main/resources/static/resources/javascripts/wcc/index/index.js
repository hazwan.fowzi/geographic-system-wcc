/* jshint smarttabs: true */

/**
 * The Index main class. 
 *
 * @class
 * @name Index
 **/
define([
	'wcc/utils/ajax-request',
	'wcc/utils/link-const'
], function(
		Request,
		LinkConst
		) {
	'use strict';
	
	var Index =  _.extend({}, /** @lends Index.prototype */ {
		/**
		 * Bind login action.
		 * 
		 * @returns {void}
		 * @private
		 */
		_bindAction: function() {
			var self = this;
			
			$("form#frm_distance").submit(function(e) {
			    e.preventDefault();

				const postcodeFrom = $('#in_dest_from').val();
				const postcodeTo = $('#in_dest_to').val();
	
				let formData = {
					'postcodeFrom': postcodeFrom,
					'postcodeTo': postcodeTo
				};
				
				Request.get(
					LinkConst.Geographic.GET_CALCULATE_DISTANCE,
					formData,
					_.bind(function(result) {
						if (result.status === Request.Result.TRUE) {
							var dataJson = JSON.parse(result.message);
							
							$('#in_ps_1_up').val(dataJson['fromPostcode']);
							$('#in_lat_1_up').val(dataJson['fromLat']);
							$('#in_long_1_up').val(dataJson['fromLong']);
							
							$('#in_ps_2_up').val(dataJson['toPostcode']);
							$('#in_lat_2_up').val(dataJson['toLat']);
							$('#in_long_2_up').val(dataJson['toLong']);
							
							var totalDistance = parseFloat(dataJson['totalDistance']);
							
							$('#in_total_distance').val(totalDistance.toFixed(2) + ' KM');
						}else{
							alert('Please enter the correct Postcode!');
						}
					}, this)
				);
			});
			
			$('#btn_submit_1').off('click').on('click', function(){
				const inputObj = {
					'postcode' : $('#in_ps_1_up').val(),
					'latitude' : $('#in_lat_1_up').val(),
					'longitude' : $('#in_long_1_up').val()
				};
				
				self._updateFormSubmit(inputObj);
			});
			
			$('#btn_submit_2').off('click').on('click', function(){
				const inputObj = {
					'postcode' : $('#in_ps_2_up').val(),
					'latitude' : $('#in_lat_2_up').val(),
					'longitude' : $('#in_long_2_up').val()
				};
				
				self._updateFormSubmit(inputObj);
			});
		},
		
		/**
		 * Init func.
		 * 
		 * @returns {void}
		 * @public
		 */
		init: function(){
			var self = this;
			console.log('js in');
			
//			self._bindDropdownItem();
			self._bindAction();
		},
		
		/**
		 * Update form submit.
		 * 
		 * @returns {void}
		 * @private
		 */
		_updateFormSubmit: function(inputObj){			
			Request.post(
				LinkConst.Geographic.POST_UPDATE_POSTCODE,
				{formData: JSON.stringify(inputObj)},
				_.bind(function(result) {
					if (result=== Request.Result.TRUE) {
						alert('Postcode detail updated!');
					}
				}, this),
				{
					contentType : Request.ContentType.UTF8_URL_ENCODED
				}
			);
		},
		
		/**
		 * Bind dropdown list attr.
		 * 
		 * @returns {void}
		 * @private
		 */
		_bindDropdownItem: function(){
			var $selectPostcode = $('#select_from, #select_to');
			$selectPostcode.empty();
			
			$selectPostcode.find('option').remove();
			var $option = $('<option></option>');
			$selectPostcode.append($option.val('').text(messages('label.postcode.choose.info')));
			
			Request.get(
				LinkConst.Geographic.GET_POSTCODE_LIST,
				{},
				_.bind(function(result) {					
							
					if(result.length>0){
						
						_.each(result, function(item) {
							$option = $('<option></option>');
									
							$selectPostcode.append(
								$option.val(item).text(item)
							);
						});
						
					}
				}, this)
			);
		}
	});
	
	return Index;
});