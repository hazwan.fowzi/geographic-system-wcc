package wcc.project.service;

import org.springframework.stereotype.Component;

@Component
public interface GeographicService {
	double getDistance(double latitude, double longitude, double latitude2, double longitude2);
	boolean updatePostcodeData(String postcode, double latitude, double longitude);
//	double calculateDistance(double latitude, double longitude, double latitude2, double longitude2);
}
