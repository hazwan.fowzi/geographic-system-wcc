package wcc.project.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wcc.project.entity.Postcodelatlng;
import wcc.project.repository.GeographicRepository;
import wcc.project.service.GeographicService;


@Service
public class GeographicServiceImpl implements GeographicService {
	
	Logger logger = LoggerFactory.getLogger(GeographicServiceImpl.class);
	
	private final static double EARTH_RADIUS = 6371; // radius in kilometers

	@Autowired private GeographicRepository geographicRepository;
		
	@Override
	public double getDistance(double latitude, double longitude, double latitude2, double longitude2) {
		double totalDistance = calculateDistance(latitude, longitude, latitude2, longitude2);
		return totalDistance;
	}
	
	private double calculateDistance(double latitude, double longitude, double latitude2, double longitude2) {
		// Using Haversine formula! See Wikipedia;
		 double lon1Radians = Math.toRadians(longitude);
		 double lon2Radians = Math.toRadians(longitude2);
		 double lat1Radians = Math.toRadians(latitude);
		 double lat2Radians = Math.toRadians(latitude2);
		 double a = haversine(lat1Radians, lat2Radians) + Math.cos(lat1Radians) * Math.cos(lat2Radians) * haversine(lon1Radians, lon2Radians);
		 double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		 return (EARTH_RADIUS * c);
	}
	
	private double haversine(double deg1, double deg2) {
	 	return square(Math.sin((deg1 - deg2) / 2.0));
	}
	private double square(double x){
		return x * x;
	}
	
	@Override
	public boolean updatePostcodeData(String postcode, double latitude, double longitude) {
		
		List<Postcodelatlng> results = geographicRepository.getLatLong(postcode);
		if (results.size()>0) {
			Postcodelatlng postcodelatlng;
			
			postcodelatlng = results.get(0);
			postcodelatlng.setLatitude(latitude+"");
			postcodelatlng.setLongitude(longitude+"");
			geographicRepository.save(postcodelatlng);
			return true;
		}
		return false;
	}
}
