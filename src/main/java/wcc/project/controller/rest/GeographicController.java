package wcc.project.controller.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import wcc.project.dto.ApiResponse;
import wcc.project.entity.Postcodelatlng;
import wcc.project.repository.GeographicRepository;
import wcc.project.service.GeographicService;
import wcc.project.service.impl.GeographicServiceImpl;

@RestController
@RequestMapping("/api/geographic")
public class GeographicController {
	Logger logger = LoggerFactory.getLogger(GeographicServiceImpl.class);
	
	@Autowired private GeographicService geographicService;
	@Autowired private GeographicRepository geographicRepository;
	
	@GetMapping("/calculate")
    public ApiResponse<Boolean> getDistanceCalculate (@RequestParam("postcodeFrom") String postcodeFrom, @RequestParam("postcodeTo") String postcodeTo) {
		
		try {
			List<Postcodelatlng> fromResults = geographicRepository.getLatLong(postcodeFrom);
			List<Postcodelatlng> toResults = geographicRepository.getLatLong(postcodeTo);
			
			if(fromResults.size()>0 && toResults.size()>0) {
				double fromLat = Double.parseDouble(fromResults.get(0).getLatitude());
				double fromLong = Double.parseDouble(fromResults.get(0).getLongitude());
				
				double toLat = Double.parseDouble(toResults.get(0).getLatitude());
				double toLong = Double.parseDouble(toResults.get(0).getLongitude());
				
				double totalDistance = geographicService.getDistance(fromLat, fromLong, toLat, toLong);
				
				String returnJson = "{\"fromPostcode\":\"" + postcodeFrom + "\",\"fromLat\":\"" + fromLat + "\",\"fromLong\":\"" + fromLong + 
						"\",\"toPostcode\":\"" + postcodeTo + "\",\"toLat\":\"" + toLat + "\",\"toLong\":\"" + toLong + "\",\"totalDistance\":\"" + totalDistance + "\"}"; 
				
				return new ApiResponse<Boolean>(true,returnJson);
			}else {
				return new ApiResponse<Boolean>(false,"no postcode");
			}
		} catch (Exception e) {
			return new ApiResponse<Boolean>(false, e.getMessage());
		}
    }
	
	@GetMapping("/postcodelist")
    public List<String> getPostcodeList () {
		
		try {
			List<String> dashboardGroupDistinct = geographicRepository.getAllPostcode();
			return dashboardGroupDistinct;
		} catch (Exception e) {
			return null;
		}
    }
	
	@PostMapping("/update")
    public boolean updatePostcodeItem (@RequestParam("formData") String postcodeFrom) {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			JsonNode detailsNode = mapper.readTree(postcodeFrom);
			String postcodeN = detailsNode.findPath("postcode").asText();
			String latitudeN = detailsNode.findPath("latitude").asText();
			String longitudeN = detailsNode.findPath("longitude").asText();
			
			String postcode = postcodeN;
			double latitude = Double.parseDouble(latitudeN);
			double longitude = Double.parseDouble(longitudeN);;
			boolean isUpdate = geographicService.updatePostcodeData(postcode, latitude, longitude);
			
			return isUpdate;
		} catch (Exception e) {
			return false;
		}
    }
}
