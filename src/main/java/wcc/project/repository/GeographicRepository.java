package wcc.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import wcc.project.entity.Postcodelatlng;

@Repository
public interface GeographicRepository extends CrudRepository<Postcodelatlng, Long>{
	
	Postcodelatlng getByPostcode(String postcode);
	
	@Query("SELECT e FROM Postcodelatlng e WHERE e.postcode LIKE :postcode")
	List<Postcodelatlng> getLatLong(@Param("postcode") String postcode);
	
	@Query("SELECT e FROM Postcodelatlng e  ORDER BY e.postcode ASC")
	@Transactional(readOnly = true)
	List<String> getAllPostcode();

}
