package wcc.project.dto;

import wcc.project.utils.StatusCode;

public class ApiResponse<T> {
    private boolean status;
    private int statusCode;
    private String message;
    private T data;

    public ApiResponse() {
        super();
    }
    
    public ApiResponse(boolean status, int statusCode, String message) {
		super();
		this.status = status;
		this.statusCode = statusCode;
		this.message = message;
	}

    public ApiResponse(boolean status) {
        super();
        this.status = status;

        if (status) {
            statusCode = StatusCode.SUCCESS.getStatusCode();
            message = StatusCode.SUCCESS.getStatusMessage();
        }
        else {
            statusCode = StatusCode.UNKNOWN_INTERNAL_ERROR.getStatusCode();
            message = StatusCode.UNKNOWN_INTERNAL_ERROR.getStatusMessage();
        }
    }

    public ApiResponse(boolean status, String message, T data) {
        super();
        this.status = status;
        this.message = message;
        this.data = data;
        this.statusCode = status ? StatusCode.SUCCESS.getStatusCode() :
                StatusCode.BAD_REQUEST.getStatusCode();
    }

    public ApiResponse(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
        this.statusCode = status ? StatusCode.SUCCESS.getStatusCode() :
                StatusCode.BAD_REQUEST.getStatusCode();
	}
    
    public ApiResponse(boolean status, T data) {
		super();
		this.status = status;
		this.data = data;
		if (status) {
            statusCode = StatusCode.SUCCESS.getStatusCode();
            message = StatusCode.SUCCESS.getStatusMessage();
        }
        else {
            statusCode = StatusCode.UNKNOWN_INTERNAL_ERROR.getStatusCode();
            message = StatusCode.UNKNOWN_INTERNAL_ERROR.getStatusMessage();
        }
	}


    public boolean isStatus() {
        return this.status;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
