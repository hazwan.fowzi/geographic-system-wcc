package wcc.project.utils;

public enum StatusCode {
    SUCCESS (200, "Success"),

    // Client error code : problem with request from client
    BAD_REQUEST (400, "Bad Request"),

    // Server error code : request is handled but process is failed
    UNKNOWN_INTERNAL_ERROR (501, "Unknown Internal Error"),
    BILLER_ERROR (502, "Error from Biller Provider");

    private Integer statusCode;
    private String statusMessage;

    private StatusCode(Integer statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
